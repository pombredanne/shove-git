git versioned `shove <https://pypi.python.org/pypi/shove>`_ store.

*shove*'s URI for git-based stores uses the form:

git://<path>

Where *<path>* is a URL path to a git repository on a local filesystem.
Alternatively, a native pathname to the repository can be passed as the
*engine* argument.